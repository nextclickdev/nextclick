<!--Add Sub_Category And its list-->
<div class="row">
	<div class="col-12">
		<h4 class="ven">Add request</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('general/support/support_queries/u/0');?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">

<input type = "hidden" name = "id" value ="<?php echo $support_requests[0]['id']; ?>">

				<div class="form-row">
					 <div class="form-group col-md-12">
						<label>Token no </label> <input type="text"
							class="form-control" name="token" placeholder="token" required="" value="<?php echo $support_requests[0]['token_no']; ?>">
						<div class="invalid-feedback">Enter token Number</div>
						<?php echo form_error('token','<div style="color:red">','</div>')?>
					</div>

					
					<div class="form-group col-md-12">
						<label>Relate no:</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select required class="form-control" name="req_id"  >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($request_type as $category):?>
    <option value="<?php echo $category['id'];?>" <?php if($category['id'] == $support_requests[0]['request_type_id']){ echo "selected";} ?> ><?php echo $category['title']?></option>
    							<?php endforeach;?>
						</select>
					</div>


					<div class="form-group col-md-12">
						<label>App Details:</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select required class="form-control" name="app_details_id"  >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($app_details as $category):?>
    								<option value="<?php echo $category['id'];?>" <?php if($category['id'] == $support_requests[0]['app_details_id']){ echo "selected";} ?>><?php echo $category['app_name']?></option>
    							<?php endforeach;?>
						</select> 
					</div>

                    <div class="form-group col-md-12">
						<label>Contact Mail</label> <input type="text"
							class="form-control" name="contact_mail" placeholder="Contact Mail" required="" value="<?php echo $support_requests[0]['email']; ?>">
						<div class="invalid-feedback">Enter MailID</div>
						<?php echo form_error('contact_mail','<div style="color:red">','</div>')?>
					</div>

					 <div class="form-group col-md-12">
						<label>Mobile</label> <input type="text"
							class="form-control" name="mobile" placeholder="mobile" required="" value="<?php echo $support_requests[0]['mobile']; ?>">
						<div class="invalid-feedback">Enter Mobile Number</div>
						<?php echo form_error('mobile','<div style="color:red">','</div>')?>
					</div>

					 <div class="form-group col-md-12">
						<label>Subject</label> <input type="text"
							class="form-control" name="subject" placeholder="subject" required="" value="<?php echo $support_requests[0]['subject']; ?>">
						<div class="invalid-feedback">Enter subject</div>
						<?php echo form_error('subject','<div style="color:red">','</div>')?>
					</div>

					
					 <div class="col col-sm col-md-12" >
          <label>Description</label>

            <textarea id="request_desc" name="req_content" class="ckeditor" rows="10" data-sample-short><?php echo $support_requests[0]['message']; ?></textarea>
           <?php echo form_error('req_content', '<div style="color:red">', '</div>');?>
         </div>
					<div class="form-group col-md-12">
						<button class="btn btn-primary mt-27 ">Submit</button>
					</div>


				</div>


			</div>
		</form>
	</div>
</div>

