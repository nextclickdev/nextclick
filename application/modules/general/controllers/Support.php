<?php

class Support extends MY_Controller
{
	public function __construct()
    {
        error_reporting(E_ERROR | E_PARSE);
        parent::__construct();
        $this->template = 'template/admin/main';
        $this->load->model('support_model');
         $this->load->model('request_model');
         $this->load->model('app_details_model');
         $this->load->library('pagination');
    }

    public function support_queries($type = 'r',$rowno = 0 )
    {
        if($type == 'r'){

        $rowperpage = $noofrows ? $noofrows : 10;

        if ($rowno != 0) {
            $rowno = ($rowno - 1) * $rowperpage;
        }
            $this->data['title'] = 'Support';
            $this->data['content'] = 'general/support';
            $this->data['nav_type'] = 'support';

        if ($this->input->post('noofrows') != NULL) {
 
            $noofrows = $this->input->post('noofrows');
            $rowperpage = $noofrows ? $noofrows : 10;

        if ($rowno != 0) {
            $rowno = ($rowno - 1) * $rowperpage;
        }
 $allcount = $this->db->query("SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s
           JOIN app_details AS ad ON s.app_details_id  = ad.id
           JOIN request_type AS rt ON s.request_type_id  = rt.id")->num_rows();
                       
            
          $this->data['support_requests'] = $this->db->query("SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s JOIN app_details AS ad ON s.app_details_id  = ad.id JOIN request_type AS rt ON s.request_type_id  = rt.id LIMIT " . $rowno . ',' . $rowperpage)->result_array();
        }
        if ($this->input->post('app_name') != NULL) {
            
             $group = $this->input->post('app_name');
 

  $allcount = $this->db->query("SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s
           JOIN app_details AS ad ON s.app_details_id  = ad.id
           JOIN request_type AS rt ON s.request_type_id  = rt.id and ad.app_name like '$group'")->num_rows();
 

     $this->data['support_requests'] = $this->db->query("SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s JOIN app_details AS ad ON s.app_details_id  = ad.id JOIN request_type AS rt ON s.request_type_id  = rt.id and ad.app_name like '$group' LIMIT " . $rowno . ',' . $rowperpage)->result_array();
        }


    if ($this->input->post('content') != NULL) {
            
             $group = $this->input->post('content');
     $allcount = $this->db->query("SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s
           JOIN app_details AS ad ON s.app_details_id  = ad.id
           JOIN request_type AS rt ON s.request_type_id  = rt.id and rt.title like '$group'")->num_rows();
     $this->data['support_requests'] = $this->db->query("SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s JOIN app_details AS ad ON s.app_details_id  = ad.id JOIN request_type AS rt ON s.request_type_id  = rt.id and rt.title like '$group' LIMIT " . $rowno . ',' . $rowperpage)->result_array();

    }

        if($this->input->post('submit') == NULL)
        { 
           $allcount = $this->db->query("SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s
           JOIN app_details AS ad ON s.app_details_id  = ad.id
           JOIN request_type AS rt ON s.request_type_id  = rt.id")->num_rows();

     $this->data['support_requests'] = $this->db->query("SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s JOIN app_details AS ad ON s.app_details_id  = ad.id JOIN request_type AS rt ON s.request_type_id  = rt.id LIMIT " . $rowno . ',' . $rowperpage)->result_array();

        }
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='page-item active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tagl_close'] = "</li>";
        $config['base_url'] = base_url() . 'general/support/support_queries/r';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;

        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['row'] = $rowno;
        $this->data['noofrows'] = $rowperpage;
        $this->_render_page($this->template, $this->data);
        } 
        else if($type == 'c'){

        	$this->form_validation->set_rules($this->support_model->rules['create_rules']);
        	 if ($this->form_validation->run() == FALSE) {

            $this->data['title'] = 'Support';
            $this->data['content'] = 'general/create_support';
            $this->data['nav_type'] = 'support';
             $this->data['request_type'] = $this->request_model->get_all();
              $this->data['app_details'] = $this->app_details_model->get_all();
            $this->_render_page($this->template, $this->data);
           } else {

           	$id = $this->support_model->insert([
                    'token_no' => $this->input->post('token'),
                    'app_details_id' => $this->input->post('app_details_id'),
                    'request_type_id'=> $this->input->post('req_id'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('req_content'),
                    'query_owner_id' => 1,
                    'created_user_id' => $this->ion_auth->get_user_id()
                    
                ]);
                redirect('general/support/support_queries/r/0', 'refresh');
           }

        }  else if($type == 'delete'){

                $id = base64_decode(base64_decode($this->input->get('id')));
                $this->support_model->delete([
                    'id' => $id
                ]); 
            redirect('general/support/support_queries/r/0', 'refresh');

         } elseif ($type == 'edit') {

     $id = base64_decode(base64_decode($this->input->get('id')));
 
            $this->data['title'] = 'Edit request';
            $this->data['content'] = 'general/edit_support';
            $this->data['type'] = 'request';
            $this->data['nav_type'] = 'support';
            $this->data['request_type'] = $this->request_model->get_all();
            $this->data['app_details'] = $this->app_details_model->get_all();
            $this->data['support_requests'] = $this->db->query("SELECT *  FROM support WHERE id = '$id'")->result_array();
            $this->_render_page($this->template, $this->data);

        } elseif ($type == 'u') {

                	 $this->support_model->update([
                    'id' => $this->input->post('id'),
                    'token_no' => $this->input->post('token'),
                    'app_details_id' => $this->input->post('app_details_id'),
                    'request_type_id'=> $this->input->post('req_id'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('req_content'),
                    'query_owner_id' => 1,
                    'created_user_id' => $this->ion_auth->get_user_id()
                ], 'id');
                redirect('general/support/support_queries/r/0', 'refresh');
        }



 

    }

}