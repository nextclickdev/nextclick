<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;

class Support extends MY_REST_Controller
{
    public $user_id = NULL;
    public function __construct()
    {
        parent::__construct();
         $this->load->model('support_model');
         $this->load->model('request_model');
         $this->load->model('app_details_model');
    }


    public function support_queries_post($type = 'r', $target = 0)
    {
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN')); 
         $_POST = json_decode(file_get_contents("php://input"), TRUE);
        $this->support_model->user_id = $token_data->id;

        if ($type == 'c') {
            $this->form_validation->set_rules($this->support_model->rules['create_rules']);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(NULL, validation_errors(), REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                  
                   $appid = $this->input->get_request_header('APP_ID');
                   $app_id = base64_decode(base64_decode($appid));
                  $token =  generate_serial_no('AZ', 3, rand(999, 9999));
            
                 $is_inserted = $this->support_model->insert([
                    'token_no' => $token,//$this->input->post('token_no'),
                    'app_details_id' => $app_id ,//$this->input->post('app_details_id'),
                    'request_type_id'=> $this->input->post('request_type_id'),
                    'mobile' => $this->input->post('mobile'),
                    'email' => $this->input->post('email'),
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('message'),
                    'created_user_id' => $token_data->id,
                    'query_owner_id' => 1 
                    ]);

                    if ($is_inserted) {
                    $this->set_response_simple(($is_inserted == FALSE) ? NULL : $is_inserted, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
                } else {
                    $this->set_response_simple(($is_inserted == FALSE) ? NULL : $is_inserted, 'Failed..!', REST_Controller::HTTP_CONFLICT, FALSE);
                }

            } 
             
        
           
        }  elseif ($type == 'd') {
            
                $supportdata = $this->support_model->get($target);
                if (! empty($supportdata) && $supportdata['created_user_id'] == $token_data->id) {
                    $this->support_model->delete([
                        'id' => $target
                    ]);
                    $this->set_response_simple(NULL, 'data has deleted..!', REST_Controller::HTTP_OK, TRUE);
                } else {
                    $this->set_response_simple(NULL, 'No privilege to  delete..!', REST_Controller::HTTP_OK, FALSE);
                }
             
        }   elseif ($type == 'u') {
             $_POST = json_decode(file_get_contents("php://input"), TRUE);        
        $this->form_validation->set_rules($this->support_model->rules['update_rules']);
        
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(NULL, validation_errors(), REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                
                   $appid = $this->input->get_request_header('APP_ID');
                   $app_id = base64_decode(base64_decode($appid));
                   $id = $this->input->post('id');
                   $query = "SELECT token_no FROM support WHERE id = '$id'";

                $this->data  = $this->db->query($query)->result_array();
        
                $is_updated = $this->support_model->update([
                    'id' => $this->input->post('id'),
                   'token_no' => $this->data[0]['token_no'],//$this->input->post('token_no'),
                    'app_details_id' => $app_id,//$this->input->post('app_details_id'),
                    'request_type_id'=> $this->input->post('request_type_id'),
                    'mobile' => $this->input->post('mobile'),
                    'email' => $this->input->post('email'),
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('message'),
                    'created_user_id' => $token_data->id,
                    'query_owner_id' => 1
                ], 'id');

                if ($is_updated) {
                    $this->set_response_simple(($is_updated == FALSE) ? NULL : $is_updated, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
                } else {
                    $this->set_response_simple(($is_updated == FALSE) ? NULL : $is_updated, 'Failed..!', REST_Controller::HTTP_CONFLICT, FALSE);
                }
            }
        }  elseif ($type == 'r') {  
            $query = "SELECT s.*,ad.app_id,ad.app_name, rt.title FROM support AS s JOIN app_details AS ad ON s.app_details_id  = ad.id JOIN request_type AS rt ON s.request_type_id  = rt.id where s.created_user_id = '$token_data->id'";

                $this->data  = $this->db->query($query)->result_array();

                $this->set_response_simple($this->data , 'Support data list', REST_Controller::HTTP_OK, TRUE); 
        }

    }

}