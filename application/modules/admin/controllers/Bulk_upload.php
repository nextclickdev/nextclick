<?php
error_reporting(E_ERROR | E_PARSE);
class Bulk_upload extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        $this->load->library('user_agent');
        if (!$this->ion_auth->logged_in())
            redirect('auth/login');

        $this->load->model('category_model');
        $this->load->model('sub_category_model');
        $this->load->model('shop_by_category_model');
        $this->load->model('brand_model');
        $this->load->model('food_menu_model');
    }

    /**
     * Categories Bulk upload
     * @author Mehar
     */
    public function category()
    {
        $zip_file = $_FILES['images_zip'];
        $excel_file = $_FILES['excel_file'];
        $base_path = dirname(BASEPATH).'/uploads/categories_zip/';
        if (! file_exists($base_path)) {
            mkdir($base_path, 0777, true);
        }
        
        $random_digit= time();
        $new_file_name = $random_digit.".zip";
        mkdir($base_path.$random_digit, 0777, true);
        
        $zip_file_path = $base_path.$random_digit.'/'.$new_file_name;
        if(copy($zip_file['tmp_name'], $zip_file_path)){
            // zip extraction
            $zip = new ZipArchive();
            if ($zip->open($zip_file_path) === TRUE) {
                $zip->extractTo($base_path.$random_digit);
                $zip->close();
                $is_updated = $this->upload_categories_excel_with_images($excel_file, $base_path, $random_digit);
            }  else {
                $this->session->set_flashdata('upload_status', ["error" => "Unable to extract ZIP"]);
            }
            
        } else {
            $this->session->set_flashdata('upload_status', ["error" => "Uploading ZIP is failed"]);
        }
        
        $this->data['title'] = 'Category Bulk';
        $this->data['content'] = 'bulk_upload/category_bulk_upload';
        $this->data['nav_type'] = 'category';
        $this->_render_page($this->template, $this->data);
    }

    public function upload_categories_excel_with_images($file, $base_path, $random_digit){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        // If file uploaded
        if(!empty($file['name'])) {
            // get file extension
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            
            if($extension == 'csv'){
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } elseif($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }
            // file path
            $spreadsheet = $reader->load($file['tmp_name']);
            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            
            // array Count
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('name', 'desc');
            $makeArray = array('name' => 'name', 'desc' => 'desc');
            $SheetDataKey = array();
            
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    }
                }
            }
            $dataDiff = array_diff_key($makeArray, $SheetDataKey);
            if (empty($dataDiff)) {
                $flag = 1;
            }
            // match excel sheet column
            if ($flag == 1) { $k = 0;
                for ($i = 2; $i <= $arrayCount; $i++) {
                    $name = $SheetDataKey['name'];
                    $desc = $SheetDataKey['desc'];
                    
                    $name = filter_var(trim($allDataInSheet[$i][$name]), FILTER_SANITIZE_STRING);
                    $desc = filter_var(trim($allDataInSheet[$i][$desc]), FILTER_SANITIZE_STRING);
                    
                    if(! empty($name)){
                        $cat_id = $this->category_model->insert([
                            'name' => $name,
                            'desc' => $desc,
                        ]);
                        
                        if ($cat_id) {
                        
                        $file_name = str_replace(array("#", "'", ";","@[^0-9a-zA-Z.]+"), '', $name);
                        $space_file_name = str_replace('  ', ' ', $file_name);
                        $image_file_name = strtolower(str_replace(' ', '_', $space_file_name)).'.jpg';
                        
                            if(file_exists($base_path.$random_digit.'/nxccategories/'.$image_file_name)){
                                    if (!file_exists('uploads/' . 'category' . '_image/')) {
                                        mkdir('uploads/' . 'category' . '_image/', 0777, true);
                                    }
                                    if (file_exists('uploads/' . 'category' . '_image/' . 'category' . '_' . $cat_id . '.jpg')) {
                                        unlink('uploads/' . 'category' . '_image/' . 'category' . '_' . $cat_id . '.jpg');
                                    }
                                    $source_image = file_get_contents($base_path.$random_digit.'/nxccategories/'.$image_file_name);
                                    file_put_contents('./uploads/category_image/'."category_" . $cat_id . ".jpg", $source_image);
                                }else {
                                    $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check name and image name"]);
                                    
                                }
                        }else{
                            $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check email and mobile"]);
                            break;
                        }
                    }
                    $this->session->set_flashdata('upload_status', ["success" => "Categories successfully imported..!"]);
                }
            } else {
                $this->session->set_flashdata('upload_status', ["error" => "Please import correct file, did not match excel sheet column"]);
            }
        }
    }

    /**
     * E-Commerce brand Bulk upload
     *
     * @author Mehar
     */
    public function brands_upload()
    {
        $zip_file = $_FILES['images_zip'];
        $excel_file = $_FILES['excel_file'];
        $base_path = dirname(BASEPATH).'/uploads/brands_zip/';
        if (! file_exists($base_path)) {
            mkdir($base_path, 0777, true);
        }
        
        $random_digit= time();
        $new_file_name = $random_digit.".zip";
        mkdir($base_path.$random_digit, 0777, true);
        
        $zip_file_path = $base_path.$random_digit.'/'.$new_file_name;
        if(copy($zip_file['tmp_name'], $zip_file_path)){
            // zip extraction
            $zip = new ZipArchive();
            if ($zip->open($zip_file_path) === TRUE) {
                $zip->extractTo($base_path.$random_digit);
                $zip->close();
                $is_updated = $this->upload_brands_with_images($excel_file, $base_path, $random_digit);
            }  else {
                $this->session->set_flashdata('upload_status', ["error" => "Unable to extract ZIP"]);
            }
            
        } else {
            $this->session->set_flashdata('upload_status', ["error" => "Uploading ZIP is failed"]);
        }
        
        $this->data['title'] = 'Brands Bulk';
        $this->data['content'] = 'bulk_upload/brands_bulk_upload';
        $this->data['nav_type'] = 'brands';
        $this->_render_page($this->template, $this->data);
    }
    
    public function upload_brands_with_images($file, $base_path, $random_digit){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        // If file uploaded
        if(!empty($file['name'])) {
            // get file extension
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            
            if($extension == 'csv'){
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } elseif($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }
            // file path
            $spreadsheet = $reader->load($file['tmp_name']);
            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            
            // array Count
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('name', 'desc');
            $makeArray = array('name' => 'name', 'desc' => 'desc');
            $SheetDataKey = array();
            
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    }
                }
            }
            //print_array($SheetDataKey);
            $dataDiff = array_diff_key($makeArray, $SheetDataKey);
            if (empty($dataDiff)) {
                $flag = 1;
            }
            // match excel sheet column
            if ($flag == 1) { $k = 0;
            for ($i = 2; $i <= $arrayCount; $i++) {
                
                $name = $SheetDataKey['name'];
                $desc = $SheetDataKey['desc'];
                
                $name = filter_var(trim($allDataInSheet[$i][$name]), FILTER_SANITIZE_STRING);
                $desc = filter_var(trim($allDataInSheet[$i][$desc]), FILTER_SANITIZE_EMAIL);
                
                if(! empty($name)){
                    $brand_id = $this->brand_model->insert([
                        'name' => $name,
                        'desc' => $desc,
                        'status' => 1
                    ]);
                         
                    if ($brand_id) {
                        $file_name = str_replace(array("#", "'", ";","@[^0-9a-zA-Z.]+"), '', $name);
                        $space_file_name = str_replace('  ', ' ', $file_name);
                        $image_file_name = strtolower(str_replace(' ', '_', $space_file_name)).'.jpg';

                        if(file_exists($base_path.$random_digit.'/nxcbrands/'.$image_file_name)){
                            if (!file_exists('uploads/' . 'brands' . '_image/')) {
                                mkdir('uploads/' . 'brands' . '_image/', 0777, true);
                            }
                            if (file_exists('uploads/' . 'brands' . '_image/' . 'brands' . '_' . $brand_id . '.jpg')) {
                                unlink('uploads/' . 'brands' . '_image/' . 'brands' . '_' . $brand_id . '.jpg');
                            }
                            $source_image = file_get_contents($base_path.$random_digit.'/nxcbrands/'.$image_file_name);
                            file_put_contents('./uploads/brands_image/'."brands_" . $brand_id . ".jpg", $source_image);
                        }else {
                            $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check name and image name"]);
                        }
                    }else{
                        $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check email and mobile"]);
                        break;
                    }
                
                    
                    $this->session->set_flashdata('upload_status', ["success" => "Brands successfully imported..!"]);
                }else{
                    $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)"]);
                    $this->data['brands'] = array('name' => $name, 'desc' => $desc);
                    break;
                }
            }
            } else {
                $this->session->set_flashdata('upload_status', ["error" => "Please import correct file, did not match excel sheet column"]);
            }
        }
    }

        /**
      * Sub_Category Bulk upload
      *
      * @author Mehar
      */
      public function sub_category_upload(){
        $zip_file = $_FILES['images_zip'];
        $excel_file = $_FILES['excel_file'];
        $base_path = dirname(BASEPATH).'/uploads/subcategory_zip/';
        if (! file_exists($base_path)) {
            mkdir($base_path, 0777, true);
        }
        
        $random_digit= time();
        $new_file_name = $random_digit.".zip";
        mkdir($base_path.$random_digit, 0777, true);
        
        $zip_file_path = $base_path.$random_digit.'/'.$new_file_name;
        if(copy($zip_file['tmp_name'], $zip_file_path)){
            // zip extraction
            $zip = new ZipArchive();
            if ($zip->open($zip_file_path) === TRUE) {
                $zip->extractTo($base_path.$random_digit);
                $zip->close();
                $is_updated = $this->upload_subexcel_with_images($excel_file, $base_path, $random_digit);
            }else {
                $this->session->set_flashdata('upload_status', ["error" => "Unable to extract ZIP"]);
            }
            
        } else {
            $this->session->set_flashdata('upload_status', ["error" => "Uploading ZIP is failed"]);
        }
        $this->data['title'] = 'SubCategory bulk upload';
        $this->data['content'] = 'admin/bulk_upload/sub_category_bulk_upload';
        $this->data['nav_type'] = 'sub_category_upload';
        $this->_render_page($this->template, $this->data);
    }
    
    public function upload_subexcel_with_images($file, $base_path, $random_digit){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        // If file uploaded
        if(!empty($file['name'])) {
            // get file extension
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            
            if($extension == 'csv'){
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } elseif($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }
            // file path
            $spreadsheet = $reader->load($file['tmp_name']);
            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            
            // array Count
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('cat_id', 'type', 'name', 'desc');
            $makeArray = array('cat_id' => 'cat_id', 'type' => 'type', 'name' => 'name', 'desc' => 'desc');
            $SheetDataKey = array();
            
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    }
                }
            }
            //print_array($SheetDataKey);
            $dataDiff = array_diff_key($makeArray, $SheetDataKey);
            if (empty($dataDiff)) {
                $flag = 1;
            }
            // match excel sheet column
            if ($flag == 1) { $k = 0;
            for ($i = 2; $i <= $arrayCount; $i++) {
                
                $cat_id = $SheetDataKey['cat_id'];
                $type = $SheetDataKey['type'];
                $name = $SheetDataKey['name'];
                $desc = $SheetDataKey['desc'];
                
                $cat_id = filter_var(trim($allDataInSheet[$i][$cat_id]), FILTER_SANITIZE_STRING);
                $type = filter_var(trim($allDataInSheet[$i][$type]), FILTER_SANITIZE_STRING);
                $name = filter_var(trim($allDataInSheet[$i][$name]), FILTER_SANITIZE_STRING);
                $desc = filter_var(trim($allDataInSheet[$i][$desc]), FILTER_SANITIZE_EMAIL);
                
                if(! empty($name)  &&  ! empty($cat_id)){
                    $sub_cat_id = $this->sub_category_model->insert([
                        'cat_id' => $cat_id,
                        'type' => $type,
                        'name' => $name,
                        'desc' => $desc,
                        'product_type_widget_status' => 2,
                        'status' => 1
                    ]);
                    
                    if ($type == 2) {
                        $shop_cat_id = $this->shop_by_category_model->insert([
                            'vendor_id' => 1,
                            'cat_id' => $cat_id,
                            'sub_cat_id' => $sub_cat_id
                        ]);
                    }
                        
                    if ($sub_cat_id) {
                        
                        $file_name = str_replace(array("#", "'", ";","@[^0-9a-zA-Z.]+"), '', $name);
                        $space_file_name = str_replace('  ', ' ', $file_name);
                        $image_file_name = strtolower(str_replace(' ', '_', $space_file_name)).'.jpg';
                        
                        if(file_exists($base_path.$random_digit.'/nxcsubcategories/'.$image_file_name)){

                            if (!file_exists('uploads/' . 'sub_category' . '_image/')) {
                                mkdir('uploads/' . 'sub_category' . '_image/', 0777, true);
                            }
                            if (file_exists('uploads/' . 'sub_category' . '_image/' . 'sub_category' . '_' . $sub_cat_id . '.jpg')) {
                                unlink('uploads/' . 'sub_category' . '_image/' . 'sub_category' . '_' . $sub_cat_id . '.jpg');
                            }
                            $source_image = file_get_contents($base_path.$random_digit.'/nxcsubcategories/'.$image_file_name);
                            file_put_contents('./uploads/sub_category_image/'."sub_category_" . $sub_cat_id . ".jpg", $source_image);
                        }else {
                            $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check name and image name"]);
                        }
                    }else{
                        $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check email and mobile"]);
                        break;
                    }
                
                    
                    $this->session->set_flashdata('upload_status', ["success" => "Sub Categories successfully imported..!"]);
                }else{
                    $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)"]);
                    $this->data['subcategory'] = array('cat_id' => $cat_id, 'type' => $type,'name' => $name, 'desc' => $desc, 'image' => $image);
                    break;
                }
            }
            } else {
                $this->session->set_flashdata('upload_status', ["error" => "Please import correct file, did not match excel sheet column"]);
            }
        }
    }
    
    /**
     * Menu Bulk upload
     *
     * @author Mehar
     */
    public function menu_upload(){
        $zip_file = $_FILES['images_zip'];
        $excel_file = $_FILES['excel_file'];
        $base_path = dirname(BASEPATH).'/uploads/menu_zip/';
        if (! file_exists($base_path)) {
            mkdir($base_path, 0777, true);
        }
        
        $random_digit= time();
        $new_file_name = $random_digit.".zip";
        mkdir($base_path.$random_digit, 0777, true);
        
        $zip_file_path = $base_path.$random_digit.'/'.$new_file_name;
        if(copy($zip_file['tmp_name'], $zip_file_path)){
            // zip extraction
            $zip = new ZipArchive();
            if ($zip->open($zip_file_path) === TRUE) {
                $zip->extractTo($base_path.$random_digit);
                $zip->close();
                $is_updated = $this->upload_menu_with_images($excel_file, $base_path, $random_digit);
            }else {
                $this->session->set_flashdata('upload_status', ["error" => "Unable to extract ZIP"]);
            }
            
        } else {
            $this->session->set_flashdata('upload_status', ["error" => "Uploading ZIP is failed"]);
        }
        $this->data['title'] = 'Menu bulk upload';
        $this->data['content'] = 'admin/bulk_upload/menu_bulk_upload';
        $this->data['nav_type'] = 'menu_upload';
        $this->_render_page($this->template, $this->data);
    }
    
    public function upload_menu_with_images($file, $base_path, $random_digit){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        // If file uploaded
        if(!empty($file['name'])) {
            // get file extension
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            
            if($extension == 'csv'){
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } elseif($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }
            // file path
            $spreadsheet = $reader->load($file['tmp_name']);
            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            
            // array Count
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('vendor_id', 'sub_cat_id', 'name', 'desc');
            $makeArray = array('vendor_id' => 'vendor_id', 'sub_cat_id' => 'sub_cat_id', 'name' => 'name', 'desc' => 'desc');
            $SheetDataKey = array();
            
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    }
                }
            }
            //print_array($SheetDataKey);
            $dataDiff = array_diff_key($makeArray, $SheetDataKey);
            if (empty($dataDiff)) {
                $flag = 1;
            }
            // match excel sheet column
            if ($flag == 1) { $k = 0;
            for ($i = 2; $i <= $arrayCount; $i++) {
                
                $vendor_id = $SheetDataKey['vendor_id'];
                $sub_cat_id = $SheetDataKey['sub_cat_id'];
                $name = $SheetDataKey['name'];
                $desc = $SheetDataKey['desc'];
                
                $vendor_id = filter_var(trim($allDataInSheet[$i][$vendor_id]), FILTER_SANITIZE_STRING);
                $sub_cat_id = filter_var(trim($allDataInSheet[$i][$sub_cat_id]), FILTER_SANITIZE_STRING);
                $name = filter_var(trim($allDataInSheet[$i][$name]), FILTER_SANITIZE_STRING);
                $desc = filter_var(trim($allDataInSheet[$i][$desc]), FILTER_SANITIZE_EMAIL);
                
                if(! empty($name)  &&  ! empty($sub_cat_id)){
                    $menu_id = $this->food_menu_model->insert([
                        'vendor_id' => $vendor_id,
                        'sub_cat_id' => $sub_cat_id,
                        'name' => $name,
                        'desc' => $desc,
                        'status' => 1
                    ]);
                         
                    if ($menu_id) {
                        $file_name = str_replace(array("#", "'", ";","@[^0-9a-zA-Z.]+"), '', $name);
                        $space_file_name = str_replace('  ', ' ', $file_name);
                        $image_file_name = strtolower(str_replace(' ', '_', $space_file_name)).'.jpg';

                        if(file_exists($base_path.$random_digit.'/nxcmenu/'.$image_file_name)){
                            if (!file_exists('uploads/' . 'food_menu' . '_image/')) {
                                mkdir('uploads/' . 'food_menu' . '_image/', 0777, true);
                            }
                            if (file_exists('uploads/' . 'food_menu' . '_image/' . 'food_menu' . '_' . $menu_id . '.jpg')) {
                                unlink('uploads/' . 'food_menu' . '_image/' . 'food_menu' . '_' . $menu_id . '.jpg');
                            }
                            $source_image = file_get_contents($base_path.$random_digit.'/nxcmenu/'.$image_file_name);
                            file_put_contents('./uploads/food_menu_image/'."food_menu_" . $menu_id . ".jpg", $source_image);
                        }else {
                            $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check name and image name"]);
                            
                        }
                    }else{
                        $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check email and mobile"]);
                        break;
                    }
                
                    
                    $this->session->set_flashdata('upload_status', ["success" => "Menus successfully imported..!"]);
                }else{
                    $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)"]);
                    $this->data['menu'] = array('vendor_id' => $vendor_id, 'sub_cat_id' => $sub_cat_id,'name' => $name, 'desc' => $desc);
                    break;
                }
            }
            } else {
                $this->session->set_flashdata('upload_status', ["error" => "Please import correct file, did not match excel sheet column"]);
            }
        }
    }
    
    
}
