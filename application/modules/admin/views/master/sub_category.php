<style>
.elementToFadeInAndOut {
    display:block;
    -webkit-animation: fadeinout 10s linear forwards;
    animation: fadeinout 10s linear forwards;
}
@-webkit-keyframes fadeinout {
  0%,100% { opacity: 0; }
  50% { opacity: 1; }
}
@keyframes fadeinout {
  0%,100% { opacity: 0; }
  50% { opacity: 1; }
}
td:nth-child(5){
	position: relative;
	width:12%;
   min-height:12px;
}
</style>
<div class="row">
	<div class="col-12">
		<div class="card-body">
			<div class="card">
			<?php if (!empty($this->session->flashdata('upload_status'))) {
                ?>
                    <div class="alert alert-success elementToFadeInAndOut">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('upload_status'); ?>
                    </div>
                <?php
                } ?>
				<?php if (!empty($this->session->flashdata('delete_status'))) {
                ?>
                    <div class="alert alert-danger elementToFadeInAndOut">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('delete_status'); ?>
                    </div>
                <?php
                } ?>
				<div class="card-header">
					<h4 class="col-7 ven1">List of Sub Categories</h4>
					<?php if($this->ion_auth_acl->has_permission('subcategory_add')):?>
						<a class="btn btn-outline-dark btn-lg " href="<?php echo base_url()?>sub_category/c" class="btn btn-primary widfldtd">Add Sub Categories</a>
						&nbsp;<a class="btn btn-outline-dark btn-lg " href="<?php echo base_url('admin/bulk_upload/sub_category_upload')?>"><i class="fa fa-plus" aria-hidden="true"></i>Sub Category Bulk Upload</a>
					<?php endif;?>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>

								<tr>
									<th>Id</th>
									<th>Created By</th>
									<th>Sub Category Name</th>
									<th>Category</th>
									<th>Description</th>
									<th>Type</th>
									<th>Image</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if($this->ion_auth_acl->has_permission('subcategory_view')):?>
								<?php if(!empty($sub_categories)):?>
    							<?php $sno = 1; foreach ($sub_categories as $sub_cat):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo (! empty($sub_cat['users']['unique_id']))? $od_category['users']['unique_id'] : 'NA' ;?></td>
    									<td><?php echo $sub_cat['name'].'['.$sub_cat['id'].']';?></td>
    									<td class="tdcolorone"><?php foreach ($categories as $category):?>
    										<?php echo ($category['id'] == $sub_cat['cat_id'])? $category['name']:'';?>
    									<?php endforeach;?></td>
    									<td class="tdcolortwo ">
											<ul class="scrollitemlist">
												<li>
										<?php echo $sub_cat['desc'];?></li>
										</ul>
									</td>
    									<td><?php echo ($sub_cat['type'] == 1)? 'Listing Sub Category' : 'Shop By Category';?></td>
    									<td><img class="img-thumb"
    										src="<?php echo base_url();?>uploads/sub_category_image/sub_category_<?php echo $sub_cat['id'];?>.jpg?<?php echo time();?>"
    										></td>
    									<td>
    									<?php if($this->ion_auth_acl->has_permission('subcategory_edit')):?>
        									<a href="<?php echo base_url()?>sub_category/edit?id=<?php echo $sub_cat['id'];?>" class=" mr-2  " > <i class="fas fa-pencil-alt"></i></a>
        								<?php endif;?>
        								<?php if($this->ion_auth_acl->has_permission('subcategory_delete')):?> 
        									<a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $sub_cat['id'] ?>, 'sub_category')"> <i class="far fa-trash-alt"></i></a>
        								<?php endif;?>	
    									</td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Sub_Category</center></h3></th></tr>
							<?php endif;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Access!</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>



