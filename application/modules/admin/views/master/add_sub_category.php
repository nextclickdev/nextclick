<div class="row">
	<div class="col-12">
		<h4 class="ven Add subcategory">Add Sub Category</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('sub_category/c');?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">

				<div class="form-row">
					<div class="form-group mb-0 col-md-3">
						<label>Sub Category Name</label> <input type="text" class="form-control" name="name" id="name" trim="" required="" placeholder="Name" <?php echo set_value('name')?>>
						<div class="invalid-feedback">Give Sub Category Name</div>
						<?php echo form_error('name','<div style="color:red">','</div>');?>
					</div>

					<div class="form-group col-md-3">
						<label for="cat_id">Category</label>
						<select  class="form-control"  required="" id="cat_id" name="cat_id">
								<option value="" selected disabled>--select--</option>
    							<?php foreach ($categories as $category):?>
    								<option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>

					<div class="form-group mb-0 col-md-3">
            			<label>Description</label> 
                        <input type="text" class="form-control" name="desc" id="desc" required="" placeholder="Description" <?php echo set_value('desc')?>>
            			<div class="invalid-feedback">Give Sub Category Name</div>
             			<?php echo form_error('desc','<div style="color:red">','</div>');?>
          			</div>
					<div class="form-group col-md-3">
						<label>Type</label>
						<select class="form-control" name="type" id="type" required="">
							<option value="" selected disabled>--select--</option>
    						<option value="1">Listing Sub Category</option>
    						<option value="2">Shop By Category</option>
						</select>
						<?php echo form_error('type','<div style="color:red">','</div>');?>
					</div>
					<div class="form-group col-md-3">
						<label>Upload Image</label> 
						<input type="file"  accept="image/jpeg, image/png" name="file" required="" value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);"> <br> 
					</div>
					<div class="form-group col-md-1">
					<img id="blah" class="textimgmotion" src="<?php echo base_url(); ?>uploads/sub_category_image/sub_category_<?php echo $sub_categories['id']; ?>.jpg?<?php echo time();?>">
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					
					</div>
					<div class="form-group col-md-2">
						<button class="btn btn-primary mt-27 mt" name = "submit" id = "submit1">Submit</button>
					</div>
				</div>


			</div>
		</form>
    </div>
 </div>