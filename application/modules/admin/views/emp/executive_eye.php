<style>
    
    img.img-thumb{
    width: 18%;
    border-radius: 12px;
}

.zoom1 {
  transition: transform .2s; 
  margin: 0 auto;
}

.zoom1:hover {
  transform: scale(1.3);
  cursor: pointer;
}

.modal-target {
  width: 300px;
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

.modal-target:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.8); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 40%;
  opacity: 1 !important;
  max-width: 70%;
}

/* Caption of Modal Image */
.modal-caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 1200px;
  text-align: center;
  color: white;
  font-weight: 700;
  font-size: 1em;
  margin-top: 32px;
}

/* Add Animation */
.modal-content, .modal-caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-atransform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.modal-close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.modal-close:hover,
.modal-close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

div#modal{
    z-index: 999;
}


</style>


<?php if($type == 'executive'){?>
  <div class="row">
    <div class="col-md-12">
    <!-- <?php if($this->ion_auth_acl->has_permission('category_create')):?>
					<a href="<?php echo base_url()?>category/c" >Add Category</a> &nbsp;
				<?php endif;?> -->
       <a style="border: 1px solid #373435;border-radius: 3px;padding: 4px;background-color: #373435;color: white;" href="<?php echo base_url('emp_list/executive');?>">User <i class="fa fa-angle-double-left"></i> Executive</a> 
    </div>
</div>
<div class="row">
	<div class="col-12">
		<h4 class="ven">Employee Details</h4>
		<div class="card-header">
				<div class="form-row">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>User Id</label>
                        <div class="uid">
                        <p><?php echo $users['id']; ?></p>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Profile</label>
                        <div class="prof">
                            <img src="<?php echo base_url();?>uploads/profile_image/profile_<?php echo $users['id']; ?>.jpg"
                            class="img-thumb zoom1 modal-target">
                        </div>
                    </div>
					<div class="form-group col-md-6">
						<label>Name</label> 
                         <div class="na">
                         <p><?php echo $users['first_name'].'  '.$users['last_name']; ?></p>
                         </div>
					</div>
					
					<div class="form-group col-md-6">
						<label>Mobile No.</label> 
                        <div class="mono">
                            <p><?php echo $users['phone']; ?></p>
                        </div>
					</div>
					<div class="form-group col-md-6">
						<label>Email ID</label>
                        <div class="emailid">
                            <p><?php echo $users['email']; ?></p>
                        </div>
					</div>
                    <div class="form-group col-md-6">
						<label>Location</label>
                        <div class="plocation">
                            <p><?php echo $users['executive_address']['location']; ?></p>
                        </div>
					</div>
                   
                    <div class="form-group col-md-6">
						<label>Aadhar Number</label>
                        <div class="aan">
                            <p><?php echo $users['executive_biometric']['aadhar']; ?></p>
                        </div>
					</div>
                    <div class="form-group col-md-3">
						<label>Aadhar Card Front</label>
                        <div class="aacf">
                            <img src="<?php echo base_url();?>uploads/aadhar_card_image/aadhar_card_front_<?php echo $users['id']; ?>.jpg"
                            class="img-thumb zoom1 modal-target" style="width:80%"> 
                        </div>
					</div>
                    <div class="form-group col-md-3">
						<label>Aadhar Card Back</label>
                        <div class="adcback">
                            <img src="<?php echo base_url();?>uploads/aadhar_card_image/aadhar_card_back_<?php echo $users['id']; ?>.jpg"
                            class="img-thumb zoom1 modal-target" style="width:80%">
                        </div>
					</div>
                    <div class="form-group col-md-6">
						<label>Bank Passbook</label>
                        <div class="bpbook">
                            <img src="<?php echo base_url();?>uploads/bank_passbook_image/bank_passbook_<?php echo $users['id'];?>.jpg"
                            class="img-thumb zoom1  modal-target" style="width:40%">
                        </div>
					</div>
                   
                    <!-- The Modal -->
<div id="modal" class="modal">
  <span id="modal-close" class="modal-close">&times;</span>
  <img id="modal-content" class="modal-content">
  <div id="modal-caption" class="modal-caption"></div>
</div>
					<!--end -->
				</div>
			</div>

      <div class="row">
		<div class="col-12">
			<form id="form-smtp" action="<?php echo base_url('vendor_profile/u/bank_details');?>" class="needs-validation form" novalidate="" method="post">
				<section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                       
                        <h2 class="card-title ven ">Bank Details</h2>
                    </header>
                    <div class="card-body">
    				<div class="form-group row">
                        <label class="col-sm-3 ">A/C Holder Name<span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="ac_holder_name" class="form-control" placeholder="A/C Holder Name" required="" value="<?php echo $bank_details['ac_holder_name']?>">
                        </div>
                        <?php echo form_error('ac_holder_name','<div style="color:red">','</div>');?>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 ">Bank Name<span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="bank_name" class="form-control" placeholder="Bank Name" required="" value="<?php  echo $bank_details['bank_name']?>">
                        </div>
                        <?php echo form_error('bank_name','<div style="color:red">','</div>');?>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 ">Bank Branch<span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="bank_branch" class="form-control" placeholder="Bank Branch" required="" value="<?php echo $bank_details['bank_branch'] ?>">
                        </div>
                        <?php echo form_error('bank_branch','<div style="color:red">','</div>');?>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 ">A/C Number<span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="ac_number" class="form-control" placeholder="A/C Number" required="" value="<?php echo $bank_details['ac_number'] ?>">
                        </div>
                        <?php echo form_error('ac_number','<div style="color:red">','</div>');?>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 ">IFSC Code<span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="ifsc" class="form-control" placeholder="IFSC Code" required="" value="<?php  echo $bank_details['ifsc']?>">
                        </div>
                        <?php echo form_error('ifsc','<div style="color:red">','</div>');?>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                        	<input type="hidden"  name="list_id" value="<?php echo $_GET['id']?>" />
                            <button class="btn btn-primary">Submit</button>
                            <input type="button" class="btn btn-default" onClick="clear_form('form-smtp')" value="Reset" />
                        </div>
                    </div>
				</div>
            
            </section></form>
		</div>
	</div>





	</div>
</div>

<script>

var modal = document.getElementById('modal');

var modalClose = document.getElementById('modal-close');
modalClose.addEventListener('click', function() { 
  modal.style.display = "none";
});

// global handler
document.addEventListener('click', function (e) { 
  if (e.target.className.indexOf('modal-target') !== -1) {
      var img = e.target;
      var modalImg = document.getElementById("modal-content");
      var captionText = document.getElementById("modal-caption");
      modal.style.display = "block";
      modalImg.src = img.src;
      captionText.innerHTML = img.alt;
   }
});

</script>
<script>
 
 //$( document ).ready(function() {

//    		var zoom = 1;
		
// 		$('.zoom').on('click', function(){
// 			zoom += 0.1;
// 			$('.target').css('transform', 'scale(' + zoom + ')');
// 		});
// 		$('.zoom-init').on('click', function(){
// 			zoom = 1;
// 			$('.target').css('transform', 'scale(' + zoom + ')');
// 		});
// 		$('.zoom-out').on('click', function(){
// 			zoom -= 0.1;
// 			$('.target').css('transform', 'scale(' + zoom + ')');
// 		}); 

// });


// $('.modal-toggle').on('click', function(e) {
//   e.preventDefault();
//   $('.modal').toggleClass('is-visible');
 //});

</script>



</div>
<?php }?>
