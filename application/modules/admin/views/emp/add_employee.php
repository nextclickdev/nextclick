<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!--Add User And its list-->
<div class="row">
	<div class="col-12">
		<h4 class="ven">Add Users</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('employee/c/0'); ?>" method="post">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>First Name</label> <input type="text" name="first_name" placeholder="First Name"
							class="form-control" required="" >
						<div class="invalid-feedback">Enter First Name?</div>
						<?php echo form_error('first_name','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-6">
						<label>Last Name</label> <input type="text" name="last_name" placeholder="Last Name"
							class="form-control" required="">
						<div class="invalid-feedback">Enter Last Name?</div>
						<?php echo form_error('last_name','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-6">
						<label>Mobile No.</label> <input type="tel"  id="mobile" name="phone"  maxlength="10" placeholder="Mobile No"
							class="form-control" required="" >
						<div class="invalid-feedback">Enter Mobile number?</div>
						<?php echo form_error('phone','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-6">
						<label>Email ID</label> <input type="email" name="email" placeholder="Email ID"
							class="form-control" required="">
						<div class="invalid-feedback">Enter Email ID?</div>
						<?php echo form_error('email','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-6">
						<label>Password</label> <input type="password" class="form-control" placeholder="Password"
							name="password" id="Password" required="">
						<?php echo form_error('password','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-6">
						<label>Confirm Password</label> <input type="password"
							class="form-control" name="confirm_password" id="ConfirmPassword" placeholder="Confirm Password"
							required="">
						<?php echo form_error('confirm_password','<div style="color:red">','</div>')?>
					</div>

					<div class="form-group col-md-6">
						<label>Role(Group)</label> <br>
						<select id="example-getting-started" class="form-control" multiple="multiple" name="role[]">
                                                    	<?php foreach ($groups as $group):?>
                                                        	<option 
                        								value="<?php echo $group['id'];?>"><?php echo $group['name']?></option>
                                                        <?php endforeach;?>
                        </select>
						<div class="invalid-feedback">Select the role for User?</div>
					</div>

					<div class="form-group col-md-12">

						<button class="btn btn-primary mt-27 " id="btnSubmit">Submit</button>
					</div>
				</div>


			</div>
		</form>