
		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4 class="col-9 ven1">List of Vendor Packages</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExportNoPagination"
							style="width: 100%;">
							<thead>
								<tr>
									<th>S.no</th>
									<th>Service</th>
									<th>Package</th>
									<th>Vendor Name</th>
									<th>Vendor Email</th>
									<th>Package start date</th>
									<th>Days</th>
								</tr>
							</thead>
							<tbody>
                        <?php $i = 1; foreach ($vendor_packages as $package):?>
						
                                <tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $package['services']['name'];?></td>
									<td><?php echo $package['packages']['title'];?></td>
									<td><?php echo $package['vendors']['name'];?></td>
									<td><?php echo $package['vendors']['email'];?></td>
									<td><?php echo $package['created_at'];?></td>
									<td><?php echo $package['packages']['days'];?></td>
									
						        </tr>
                          <?php endforeach;?>
                        </tbody>
						</table>
					</div>
					<!-- Paginate -->
                    	<div class="row  justify-content-center">
                    		<div class=" col-12" style='margin-top: 10px;'>
                               <?= $pagination; ?>
                            </div>
                    	</div>
				</div>
			</div>
		</div>
	</div>
</div>
