<div class="card-body">
    <div class="card">
        <div class="card-header">
            <h4 class="col-9 ven1">Delivery Partner Payment Distribution</h4>
            <label class="right">Est. Total: Rs. <?php ((float) $total_payout >0) ? print(number_format((float)$total_payout, 2, '.', '')) : print '0' ; ?></label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <?php if (!empty($delivery_partner_payouts)) { ?>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>S No.</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Est. Account Status</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            foreach ($delivery_partner_payouts as $payout) { ?>
                                <tr>
                                    <td> <?php echo $i; ?> </td>
                                    <td> <?php echo $payout['id'] ?> </td>
                                    <td> <?php echo $payout['first_name']." ".$payout['last_name'] ?> </td>
                                    <td> <?php echo $payout['external_id'] ? "<span style='color: green'>Can be Processed</span>" : "<span style='color: red'>Cannot be Processed</span>" ?> </td>
                                    <td> <?php echo $payout['wallet'] ?> </td>
                                </tr>
                            <?php $i++;
                            } ?>
                        </tbody>
                    </table>
                    <form role="form" method="post" action="<?php echo site_url() ?>admin/delivery_partner/process_payouts">
                        <button class="btn btn-primary right">Process</button>
                    </form>
                <?php } else { ?>
                    <div class="alert alert-info" role="alert">
                        <strong>No Vendors Found!</strong>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>