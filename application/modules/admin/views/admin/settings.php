<div class="container">
    <div class="row">
        <div class="col-md-12" style="">
            <form id="form_site_settings" action="<?php echo base_url('settings/site');?>" method="post" class="needs-validation reset" novalidate="" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">System Settings</h2>
                    </header>
                    <div class="card-body">

                        <div class="form-group row">
                            <label class="col-sm-3 ">System Name<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="system_name" class="form-control" placeholder="System Name" required="" value="<?php echo $this->setting_model->where('key', 'system_name')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">System Name?</div>
                            <?php echo form_error('system_name','<div style="color:red">','</div>');?>
                                <input type="hidden" name="id" value="">
                                <br>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">System Title <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="system_title" class="form-control" placeholder="System Title " required="" value="<?php echo $this->setting_model->where('key','system_title')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">System Title ?</div>
                            <?php echo form_error('system_title','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Mobile Number<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="number" name="mobile" class="form-control" placeholder="Mobile Number" required="" value="<?php echo $this->setting_model->where('key','mobile')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Mobile Number?</div>
                            <?php echo form_error('mobile','<div style="color:red" "margin_left=100px">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Address<span class="required">*</span></label>
                            <div class="col-sm-9 ">
                                <input type="text" class="form-control" style=" height: 70px " name="address" value=" <?php echo $this->setting_model->where('key','address')->get()['value'];?>" >
                               
                            </div>
                            <div class="invalid-feedback">Address?</div>
                            <?php echo form_error('address','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Facebook Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="facebook" class="form-control" placeholder="Facebook Link" value="<?php echo $this->setting_model->where('key','facebook')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Facebook Link?</div>
                            <?php echo form_error('facebook','<div style="color:red ">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Twiter Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="twiter" class="form-control" placeholder="Twiter Link" value="<?php echo $this->setting_model->where('key','twiter')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Twiter Link?</div>
                            <?php echo form_error('twiter','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Youtube Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="youtube" class="form-control" placeholder="Youtube Link" value="<?php echo $this->setting_model->where('key','youtube')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Youtube Link?</div>
                            <?php echo form_error('youtube','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Skype Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="skype" class="form-control" placeholder="Skype Link" value="<?php echo $this->setting_model->where('key','skype')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Skype Link?</div>
                            <?php echo form_error('skype','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Pinterest Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="pinterest" class="form-control" placeholder="Pinterest Link"  value="<?php echo $this->setting_model->where('key','pinterest')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Pinterest Link</div>
                            <?php echo form_error('mobile','<div style="color:red">','</div>');?>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-3 ">Lead Allocation Time(In Minutes)</label>
                            <div class="col-sm-9">
                                <input type="text" name="lead_allocation_time" class="form-control" placeholder="Lead Allocation Time(In Minutes)"  value="<?php echo $this->setting_model->where('key','lead_allocation_time')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Pinterest Link</div>
                            <?php echo form_error('lead_allocation_time','<div style="color:red">','</div>');?>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form_site_settings')" value="Reset" />
                            </div>
                        </div>

                    </div>
            
            </section>
            </form>
        </div>
        <div class="col-md-6">
            <form id="form_sms" action="<?php echo base_url('settings/');?>" class="needs-validation" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">SMS Settings</h2>
                    </header>
                    <br>
                    <div class="card-body">

                        <div class="form-group row">
                            <label class="col-sm-4">Username <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="sms_username" class="form-control" placeholder="Username" required="" value="<?php echo $this->setting_model->where('key','sms_username')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">sms_username?</div>
                            <?php echo form_error('sms_username','<div style="color:red">','</div>');?>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label class="col-sm-4">Sender <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="sms_sender" class="form-control" placeholder="Sender" required="" value="<?php echo $this->setting_model->where('key','sms_sender')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">sms_sender?</div>
                            <?php echo form_error('sms_sender','<div style="color:red">','</div>');?>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label class="col-sm-4">Hash Key <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="sms_hash" class="form-control" placeholder="Hash Key" required="" value="<?php echo $this->setting_model->where('key','sms_hash')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Hash Key?</div>
                            <?php echo form_error('sms_hash','<div style="color:red">','</div>');?>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form_sms')" value="Reset" />
                            </div>
                        </div>
                    </div>
            
            </section>
        </form>
        </div>

        <div class="col-md-6">
            <form id="form-smtp" action="<?php echo base_url('settings/smtp');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">SMTP Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-4">SMTP Port <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="smtp_port" class="form-control" placeholder="SMTP Port" required="" value="<?php echo $this->setting_model->where('key','smtp_port')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">smtp_port?</div>
                            <?php echo form_error('smtp_port','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">SMTP Host<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="smtp_host" class="form-control" placeholder="SMTP Host" required="" value="<?php echo $this->setting_model->where('key','smtp_host')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">smtp_host?</div>
                            <?php echo form_error('smtp_host','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">SMTP Username<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="smtp_username" class="form-control" placeholder="SMTP Username" required="" value="<?php echo $this->setting_model->where('key','smtp_username')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">smtp_username?</div>
                            <?php echo form_error('smtp_username','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">SMTP Password<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="smtp_password" class="form-control" placeholder="SMTP Password" required="" value="<?php echo $this->setting_model->where('key','smtp_password')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">smtp_password?</div>
                            <?php echo form_error('smtp_password','<div style="color:red">','</div>');?>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-smtp')" value="Reset" />
                            </div>
                        </div>
                    </div>
            
            </section></form>
        </div>
        <div class="col-md-6">
            <form id="form-smtp" action="<?php echo base_url('site_logo/logo');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">Logo</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3 ">Logo</label>
                            <div class="col-sm-9">
                            <input type='file' name="file" class="form-control" onchange="news_image(this);" />
                       <?php echo form_error('file', '<div style="color:red">', '</div>');?>
                       <br><br/>
                          <img style="width:30%;" id="blah" src="<?php echo base_url(); ?>assets/img/logo.png"  alt="Logo" />
                        </div>
                    </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-smtp')" value="Reset" />
                            </div>
                        </div>
                    </div>
            
            </section></form>
        </div>
        <div class="col-md-6">
            <form id="form-smtp" action="<?php echo base_url('site_logo/favicon');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">Favicon</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3 ">Favicon</label>
                            <div class="col-sm-9">
                            <input type='file' name="file" class="form-control" onchange="news_image(this);" />
                       <?php echo form_error('file', '<div style="color:red">', '</div>');?>
                       <br><br/>
                          <img id="blah" src="<?php echo base_url(); ?>assets/img/favicon.png" style="height: 30px;width: 30px !important;" alt="Favicon" />
                        </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-smtp')" value="Reset" />
                            </div>
                        </div>
                    
                    </div>
            
            </section></form>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-6">
            <form id="form-smtp" action="<?php echo base_url('settings/payment');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">Payment Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-5">Pay per vendor<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="pay_per_vendor" class="form-control" placeholder="Pay per vendor" required="" value="<?php echo $this->setting_model->where('key','pay_per_vendor')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Pay per vendor?</div>
                            <?php echo form_error('pay_per_vendor','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5">Vendor validation count<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="vendor_validation" class="form-control" placeholder="Vendor validation count" required="" value="<?php echo $this->setting_model->where('key','vendor_validation')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Vendor validation count?</div>
                            <?php echo form_error('vendor_validation','<div style="color:red">','</div>');?>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-smtp')" value="Reset" />
                            </div>
                        </div>
                    </div>
            
            </section></form>
        </div>
        <div class="col-md-6">
            <form id="form-smtp" action="<?php echo base_url('settings/order_payment');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">Orders Payment Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-5">Pay per Order<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="pay_per_order" class="form-control" placeholder="Pay Per Order" required="" value="<?php echo $this->setting_model->where('key','pay_per_order')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Pay Per Order?</div>
                            <?php echo form_error('pay_per_order','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5">Order Validation Count<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="order_validation" class="form-control" placeholder="Order Validation Count" required="" value="<?php echo $this->setting_model->where('key','order_validation')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Order Validation Count?</div>
                            <?php echo form_error('order_validation','<div style="color:red">','</div>');?>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-smtp')" value="Reset" />
                            </div>
                        </div>
                    </div>
            
            </section></form>
        </div>
        
             <div class="col-md-6">
            <form id="form-news" action="<?php echo base_url('settings/news');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">News Payment Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-5">Pay per News<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="pay_per_news" class="form-control" placeholder="Pay Per News" required="" value="<?php echo $this->setting_model->where('key','pay_per_news')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Pay Per Order?</div>
                            <?php echo form_error('pay_per_news','<div style="color:red">','</div>');?>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-news')" value="Reset" />
                            </div>
                        </div>
                    </div>
            
            </section></form>
        </div>
        
        
    <!--MAX AMOUNT FOR CASH ON DELIVERY ORDERS-->

        <div class="col-md-6">
            <form id="form-news" action="<?php echo base_url('settings/cod');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">COD Max Amount Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-5">Max Amount Per Order<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="max_amount" class="form-control" placeholder="Max Amount Per Order" required="" value="<?php echo $this->setting_model->where('key','max_amount')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Max Amount Per Order?</div>
                            <?php echo form_error('max_amount','<div style="color:red">','</div>');?>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-news')" value="Reset" />
                            </div>
                        </div>
                    </div>
            
            </section>
        </form>
        </div>

      
        <!--start ramkrishna-->
        <div class="col-md-6">
            <form id="form-news" action="<?php echo base_url('settings/orders');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">Order Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-5">Automatic confirmation time<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="order_confirmation_time" class="form-control" placeholder="Automatic confirmation time" required="" value="<?php echo $this->setting_model->where('key','order_confirmation_time')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Pay Per Order?</div>
                            <?php echo form_error('pay_per_news','<div style="color:red">','</div>');?>
                        </div>
                         <div class="form-group row">
                            <label class="col-sm-5">Automatic cancellation time<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="order_cancellation_time" class="form-control" placeholder="Automatic cancellation time" required="" value="<?php echo $this->setting_model->where('key','order_cancellation_time')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Pay Per Order?</div>
                            <?php echo form_error('pay_per_news','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5">Customer penalty(in %)<span class="required">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="customer_penalty_in_percentage" class="form-control" placeholder="Customer penalty" required="" value="<?php echo $this->setting_model->where('key','customer_penalty_in_percentage')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">Pay Per Order?</div>
                            <?php echo form_error('customer_penalty_in_percentage','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5">Wide area search</label>
                            <div class="col-sm-7">
                                <input type="checkbox" name="wide_area_search" <?php echo $this->setting_model->where('key','wide_area_search')->get()['value']==1 ? 'checked':'' ;?>  data-toggle="toggle" data-style="ios" data-on="ON" data-off="OFF" data-onstyle="success" data-offstyle="danger">
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-news')" value="Reset" />
                            </div>
                        </div>
                    </div>
            </section>
            </form>
        </div>
        <!--end ramakrishna -->


        <div class="col-md-6">
            <form id="form-smtp" action="<?php echo base_url('settings/bank');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">Bank Details</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-4">UPI ID <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="bank_upi_id" class="form-control" placeholder="UPI ID" required="" value="<?php echo $this->setting_model->where('key','bank_upi_id')->get()['value']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">Bank<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="bank_name" class="form-control" placeholder="Bank" required="" value="<?php echo $this->setting_model->where('key','bank_name')->get()['value']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">Account No.<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="bank_account_no" class="form-control" placeholder="Account No." required="" value="<?php echo $this->setting_model->where('key','bank_account_no')->get()['value']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">IFSC Code<span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="bank_ifsc_code" class="form-control" placeholder="IFSC Code" required="" value="<?php echo $this->setting_model->where('key','bank_ifsc_code')->get()['value']?>">
                            </div>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
            
            </section></form>
        </div>


    <!-- start ramakrishna start 11/11/2021 -->
    <div class="col-md-7">
            <form id="form-news" action="<?php echo base_url('settings/delivery_partner_security_deposit');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title ven">Delivery Boy Fixed Deposit Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group">
                        <?php foreach ($vechile as $item):?>
                            <div class="form-group row">
                                <label class="col-sm-4"><?php echo $item['name']?> <span class="required">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"
                                        name="vehicle_output[]" type="number"
                                     placeholder="Security Deposit Amount" 
                                    required="" value="<?php echo $item['security_deposited_amount']?>">
                                </div>
                            </div>
                        <?php endforeach;?>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-news')" value="Reset" />
                            </div>
                        </div>
                    </div>
            
            </section>
        </form>
        </div>
    <!-- end ramakrishna 11/11/2021 -->



    </div>
</div>
<style>
    #editor{
  padding: 0.4em 0.4em 0.4em 0;

}
</style>





