<!DOCTYPE html>
<html lang="en" ><head>
<meta charset="UTF-8"> 
<link rel="apple-touch-icon" type="image/png" href="https://static.codepen.io/assets/favicon/apple-touch-icon-5ae1a0698dcc2402e9712f7d01ed509a57814f994c660df9f7a952f3060705ee.png" />
<meta name="apple-mobile-web-app-title" content="NextClickBusiness">

<link rel="shortcut icon" type="image/x-icon" href="https://static.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico" />

<link rel="mask-icon" type="" href="https://static.codepen.io/assets/favicon/logo-pin-8f3771b1072e3c38bd662872f6b673a722f4b3ca2421637d5596661b4e2132cc.svg" color="#111" />
<title>NextClickBusinessApp</title>
<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' />
<link rel='stylesheet' href='https://pro-next.fontawesome.com/releases/v5.7.2/css/all.css' />
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700' />
<link rel='stylesheet' href='https://getbootstrap.com/docs/4.3/examples/carousel/carousel.css' />
<meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1,
      shrink-to-fit=no" name="viewport">
  <title>Vendor data submission-Next Click</title>
  <script src="env.js?ver=1.1"></script>  
    <script type="text/javascript" src="simpleLoader.js?ver=1.1"></script>  
    <script type="text/javascript" src="init.js?ver=1.1"></script> 
  <!-- General CSS Files -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="http://vendor.nextclick.in/assets/home/css/contact-form.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='shortcut icon' type='image/x-icon' href='http://vendor.nextclick.in/assets/img/favicon.png' />
<script type="text/javascript">
    var base_url = "http://vendor.nextclick.in/";
</script>
<style>
body {
  font-family: 'Roboto Condensed', sans-serif !important;
  font-size:16px;
  line-height:1.42857143;
  padding-top:0;
}
hr {
  margin:0;
}
header h6 i {
    float: left;
    font-size: 24px;
    margin: 0;
    margin-top: 0px;
    margin-right: 0px;
    margin-right: 10px;
    margin-top: 5px;
}
header h6 span {
    overflow: hidden;
    display: block;
    float: left;
    font-size: 12px;
    line-height: 18px;    
}
.bd-placeholder-img {
  background: linear-gradient(to right, #5d2d88, #ea4c11);
}
#default2.fixed-top {
   position:fixed;
   top:0; left:0;
   width:50%;
   background-color: #222;
}
#logo-small img{
  max-height:100px;
}
#logo-small
{
  display:none;
}
img.logo {
  max-width:10%;
}
div {
        /*max-width: 1020px;*/
        min-width: 320px;
    }
div.a {
  text-align: left; /* For Edge */
  -moz-text-align-last: left; /* For Firefox prior 58.0 */
  text-align-last: left;
}
.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
</style>
</head>
<body>
  <header class="top-header">
    <div class="container">
      <div class="row" width:100%>
        <div class="col-xs-2 header-logo">
          <h1><a href="index.html"></a></h1>
        </div>
        <div class="col-md-5">
          <nav class="navbar navbar-default">
            <div class="container-fluid nav-bar">
              <div class="navbar-header">
              </div>
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>

  <section class="slider" id="home">
    <main role="main">
    <center><img src="<?php echo base_url();?>assets/img_for_vendor/ncklogo.jpg" class="img-responsive logo"></center>
    <center><b><h1 style="color:#FF4500;">Business.Nextclick.in</h1></b></center>
    <div class="container-fluid">
      <div class="row">
          <div id="carouselHacked" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="header-backup"></div>
          <div class="carousel-inner" role="listbox">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
       <img src="<?php echo base_url();?>assets/img_for_vendor/Slide_1.jpg" width="50%" height="30%">
      </div>
      <div class="carousel-item">
        <img src="<?php echo base_url();?>assets/img_for_vendor/Slide_2.jpg" width="50%" height="30%">
      </div>
      <div class="carousel-item">
        <img src="<?php echo base_url();?>assets/img_for_vendor/Slide_3.jpg" width="50%" height="30%">
      </div>
      <div class="carousel-item">
        <img src="<?php echo base_url();?>assets/img_for_vendor/Slide_4.jpg" width="50%" height="30%">
      </div>
      <div class="carousel-item">
        <img src="<?php echo base_url();?>assets/img_for_vendor/Slide_5.jpg?" width="50%" height="30%">
      </div>
      <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img">
      <defs>
        <linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(93, 45, 136);stop-opacity:1" />
        <stop offset="0%" style="stop-color:rgb(234, 76, 17);stop-opacity:1" />
      </linearGradient>
      </defs>
          <rect width="50%" height="50%" fill="url(#grad1)"/></svg>
        <div class="container">
          <div class="carousel-caption">
            <h1></h1>
            <p></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#777"/></svg>
        <div class="container">
          <div class="carousel-caption text-right">
            <h1></h1>
            <p></p>
            <p><a class="btn btn-lg btn-primary" href="#" role="button"></a></p>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
        </div>
      </div>
    </div>
 
</section>
<section class="about text-left" id="about">
  <div class="container">
      <div class="column">
        <h2 style="color:green;"><div class="_49wt"><h3 class="_ez1 _80jp _8xo5 _8p_b _34g8 _9d10" style="color:#155257"></h3><div style="color:#155257;" class="_38io _8p_9 _30jd _8xoh _82qw"><p style="color:#155257;" class="_5yvq">NEXTCLICK for Business</p></div></h2>
      </div><br>
      <div class="column">
        <h3 style="color:green;"><div class="_49wt"><h3 class="_ez1 _10jp _2xo3_6p_b _25g8 _5d10" style="color:#155257"></h3><div style="color:#155257;" class="_30io _4p_9 _5jd _6xoh _40qw"><p style="color:#155257;" class="_5yvq">Joins the hands with Nextclick,let your business grow</p></div></h3>
      </div>
      <div class="column">
      <h5><div class="_49wt"><h3 class="_ez1 _80jp _8xo5 _8p_b _34g8 _9d10" style="color:darkblue"></h3><div style="color:darkblue;" class="_38io _8p_9 _30jd _8xoh _82qw">
        <p style="color:darkblue;">
          <div width="1000" height="600">
            During this pandemic many of the business  personsvendors have been facing the problems to increase their business. In order to increase their potential business Next click will provide the CRM for the vendors with an absolute free cost. We are going to help the vendors during this challenging time.
          </div>
        </p>
      </h5>
    </div> 
  </div>
    <div><img src="<?php echo base_url();?>assets/img_for_vendor/Food.jpg" width="325" height="300">
         <img src="<?php echo base_url();?>assets/img_for_vendor/Groceries.jpg" width="325" height="300">
         <img src="<?php echo base_url();?>assets/img_for_vendor/Hospital.jpg" width="325" height="300"></div></br></br>
  </section>
   
      <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <div class="main-content">
         <!--Important-->
  <section class="section-bg" style="background-image: url(https://i.ibb.co/9p3Cnk9/slider-2.jpg);"  data-scroll-index="3">
          <div class="overlay pt-80 pb-80">
            <div class="container">
               <div class="row">
                    <div class="col-lg-5 d-flex align-items-center">
                        <div class="coantact-info">

                            <h2 class="contact-title">Have Any Questions?</h2>
                            <div class="logo-image">
                              <img alt="image" src="<?php echo base_url()?>assets/img/logo.png" class="header-logo" />
                            </div>
                            <ul class="contact-info">
                                <li>
                                  <div class="info-left">
                                    <!--   <i class="fa fa-phone"></i> --> <a  class="whats-app" href="https://api.whatsapp.com/send?phone=<?php echo $this->setting_model->where('key','mobile')->get()['value'];?>&text=Hi%20There!" target="_blank"><i class="fa fa-whatsapp my-float"></i></a>
                                    <div class="info-right">
                                    <!--   <h4><?php //echo $this->setting_model->where('key','mobile')->get()['value'];?></h4> -->
                                      <!-- <a  class="whats-app" href="https://api.whatsapp.com/send?phone=<?php// echo $this->setting_model->where('key','mobile')->get()['value'];?>&text=Hi%20There!" target="_blank"><i class="fa fa-whatsapp my-float"></i></a> -->
                                      <h4>Reach Us By One Click!!</h4>
                                  </div>
                                  </div>
                                  
                                </li>
                                <li>
                                  <div class="info-left">
                                      <i class="fa fa-inbox"></i>
                                      <div class="info-right">
                                      <h4><?php echo $this->setting_model->where('key','system_email')->get()['value'];?></h4>
                                  </div>
                                  </div>
                                  
                                </li>
                                <li>
                                  <div class="info-left">
                                      <i class="fa fa-address-book"></i>
                                      <div class="info-right">
                                      <h4><?php echo $this->setting_model->where('key','address')->get()['value'];?></h4>
                                  </div><br>
                                  <div align="center">
                            <div class="col-md-12">
                            <input type="hidden" name="id" value="NCV0200"/>
                              <center>
                                <button  class="btn btn-info" onclick="document.location='https://nextclick.in/'">
                                    <i class="fa fa-users"></i>
                                ABOUT US </button>
                              </center>
                          </div>
                        </div>
                                  </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex align-items-center">
                            <div class="contact-form">
                                  <?php if(! empty($this->session->flashdata('success'))){?>
                      <div class="alert alert-success" role="alert">
                        <p><?php echo $this->session->flashdata('success');?></p>
                      </div>                                  
                                  <?php }elseif(! empty($this->session->flashdata('failed'))){?>
                                  <div class="alert alert-danger" role="alert">
                        <p><?php echo $this->session->flashdata('failed');?></p>
                      </div> 
                                  <?php }else {}?>
                                        <!--Contact Form-->
                                        <form id='contact-form' method='POST' action="<?php echo base_url()?>details_by_vendor/c/0">
                                            <div class="row">
                                               <div class="col-md-12">
                                                  <div class="form-group">
                                                     <input type="text" name="customer_name" class="form-control" id="your-name" placeholder="Enter Your Name *" required="required" value="<?php echo set_value('customer_name')?>">
                                                  </div>
                                                  <?php echo form_error('customer_name', '<div style="color:red">', '</div>');?>
                                               </div>
                                               <div class="col-md-12">
                                                  <div class="form-group">
                                                     <input type="text" name="shop_name" class="form-control" id="your-name" placeholder="Shop/Busniess Name *" required="required" value="<?php echo set_value('shop_name')?>">
                                                  </div>
                                                  <?php echo form_error('shop_name', '<div style="color:red">', '</div>');?>
                                               </div>
                                               <div class="col-md-12">
                                                  <div class="form-group">
                                                     <input type="text" name="landmark" class="form-control" id="your-name" placeholder="Area *" required="required" value="<?php echo set_value('landmark')?>">
                                                  </div>
                                                  <?php echo form_error('landmark', '<div style="color:red">', '</div>');?>
                                               </div>
                                               <div class="col-md-12">
                                                  <div class="form-group">
                                                     <input type="email" name="email" class="form-control" id="email" placeholder="Enter Your Email *" required="required" value="<?php echo set_value('email')?>">
                                                  </div>
                                                  <?php echo form_error('email', '<div style="color:red">', '</div>');?>
                                               </div>
                                               <div class="col-md-12">
                                                  <div class="form-group">
                                                     <input type="text" name="phone" class="form-control" id="your-name" placeholder="Enter Your Mobile Number *" required="required" value="<?php echo set_value('phone')?>">
                                                  </div>
                                                  <?php echo form_error('phone', '<div style="color:red">', '</div>');?>
                                               </div>
                                               <div class="col-md-12">
                                                     <div class="form-group">
                                                        <label >Categories*</label>
                                                        <select required class="form-control" name="cat_id"  >
                                                           <option value="0" selected disabled>--select--</option>
                                                           <?php foreach ($categories as $category):?>
                                                           <option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
                                                           <?php endforeach;?>
                                                        </select>
                                                     </div>
                                                     <?php echo form_error('cat_id', '<div style="color:red">', '</div>');?>
                                                  </div>
                                            <div class="col-md-12">
                                                  <div class="form-group">
                                                   <label >Address *</label>
                                                     <textarea rows="3" cols="15" class="form-control" name="address"><?php echo (!empty(set_value('address')))? set_value('address'): ""?></textarea>
                                                  </div>
                                                  <?php echo form_error('address', '<div style="color:red">', '</div>');?>
                                               </div>
                                                 <div class="col-md-12"> <input type="hidden" name="id" value="<?php echo (! empty($_GET['id']))? $_GET['id'] : $this->session->userdata('unique_id');?>" />
                                <button  class="btn-big btn btn-bg">
                                    Send Us <i class="fa fa-arrow-right"></i>
                                </button>
                                <a  class="whats-app" href="https://api.whatsapp.com/send?phone=<?php echo $this->setting_model->where('key','mobile')->get()['value'];?>&text=Hi%20There!" target="_blank">
                                <i class="fa fa-whatsapp my-float" style="color: green;font-size: 79px;margin-left:300px;margin-top: -76px;"></i></a>
                            </div>
                                             
                                               
                                            </div>
                                        </form>
                                    </div>
                    </div>
               </div>
           </div>
              </div>
        </section>
        <!--important end-->
      </div>
    </div>
  
  <section class="about text-left" id="about">
    <div class="container"></div>
   <h3 style="color:green;"><div class="_49wt"><h3 class="_ez1 _80jp _8xo5 _8p_b _34g8 _9d10" style="color:#20B2AA;"></h3><div style="color:#20B2AA;" class="_38io _8p_9 _30jd _8xoh _82qw"><h3><p style="color:#20B2AA;" class="_5yvq">How the programme can help</p></h3></div></div>
 </h3>
      <h3 style="color:green;"><div class="_49wt"><h3 class="_ez1 _80jp _8xo5 _8p_b _34g8 _9d10" style="color:#155257"></h3><div style="color:#155257;" class="_38io _8p_9 _30jd _8xoh _82qw"></div></div>
    </h3>
     
      <h3 style="color:green;"><div class="_49wt"><h3 class="_ez1 _80jp _8xo5 _8p_b _34g8 _9d10" style="color:#20B2AA;"></h3><div style="color:#20B2AA;" class="_38io _8p_9 _30jd _8xoh _82qw"><h3><p style="color:#155257;" class="_5yvq">Nextclick will increase your potential business not only in your area but also entire the world.</p></h3></div></div>
    </h3>
      <h3 style="color:green;"><div class="_49wt"><h3 class="_ez1 _80jp _8xo5 _8p_b _34g8 _9d10" style="color:#155257"></h3><div style="color:#155257;" class="_38io _8p_9 _30jd _8xoh _82qw"></div></div>
    </h3>
      <h4>
      <ol>
        <p><div align="left">
        <li>It will increase your new customers day by day</li>
        <li>It will crease the wide range of customers from all over
            the world. Let your business go wide with this CRM</li>
        <li>You can explore your ads with this CRM</li>
        <li>You can promote your business throughout the world</li></div></p>
      </ol>
      </h4><br>
      </div>
        <p><div align="left"><h3 style="color:#20B2AA;">Is my business eligible?</h3></div></p>
        <h4>
      <ol>
        <p><div align="left">
        <li>Yes,Nextclick will provide the CRM to any business
          person irrespective of business category.</li>
        <li>Eligible criteria for the vendor is no bar.. Expect wines
          shops.</li>
         <li> Be in a near location with CRM of Nextclick</li>
        </div></p>
      </ol>
  </h4><br>
  <p><div align="left"><h3 style="color:#20B2AA">How do I apply?</h3></div></p>
  <p><div align="left">Please provide the information in the beginning
                       page and submit it.</div></p><br>
  <p><div align="center"><h3 style="color:#20B2AA">Marketing Resources</h3></div></p><br>
  <p><div align="left"><h3 style="color:#20B2AA">Marketing on Nextclick</h3></div></p>
    <h4>
     <ul>
        <li>Set up your shop</li>
        <li>Set up your business ad</li>
      </ul>
    </h4>
  <p><div align="left"><h3 style="color:#20B2AA">Marketing on social media</h3></div></p>
    <h4>
     <ul>
        <li>We will share your business profile throughout the social media</li>
      </ul>
    </h4>
  <p><div align="left"><h3 style="color:#20B2AA">Audience Network</h3></div></p>
  <p><div align="left"><h3 style="color:#20B2AA">Marketing on WhatsApp</h3></div></p> 
    <h4>
     <ul>
        <li>Marketing on WhatsApp</li>
        <li>Get started with WhatsApp</li>
      </ul>
    </h4>
  <p><div align="left"><h3 style="color:#20B2AA">Advertising</h3></div></p>
    <h4>
      <ul>
        <li>Advertising</li>
        <li>Choose an objective</li>
        <li>Choose an audience</li>
        <li>Choose a format</li>
        <li>Choose a budget</li>
        <li>Measure your ads</li>
      </ul>
    </h4>
  <p><div align="left"><h3 style="color:#20B2AA">News</h3></div></p>
     <ul>
        <li>Subscribe to news letter</li>
      </ul>
  <p><div align="left"><h3 style="color:#20B2AA">Advertiser help center</h3></div></p>
  <p><div align="left"><h3 style="color:#20B2AA">Support</h3></div></p>
  <p><div align="left"><h3 style="color:#20B2AA">Reach business goal</h3></div></p>
    <h4>
      <ul>
        <li>Increase brand awareness</li>
        <li>Promote your shop</li>
        <li>Increase online sales</li>
        <li>Promote your local business</li>
        <li>Generate leads</li>
        <li>Retargeting</li>
      </ul>
    </h4><br>
     <p><div align="center"><h3 style="color:#20B2AA">OUR CATEGORIES</h3></div></p>
    <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/groceries_vendor.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/food_vendor.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/hospital_vendor.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/medicines.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
         <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/shopping.jpg" style="width:450px; height:300px" />
 
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
          <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/beuty_zone.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/kids.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/automobiles.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/electronics.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/tours.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/packages.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
           <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/emergency_need.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
  <div>
    <center>
      <h1>
        <img alt="image" src="<?php echo base_url()?>assets/img_for_vendor/doctor.jpg" style="width:450px; height:300px" />
      </h1>
    </center>
  </div>
</div><br><br>
<p><div align="left"><h3 style="color:#20B2AA">Vendor Management:</h3></div></p><br>
<center>
  <div class="left">
    <div class="column">
      <img src="<?php echo base_url();?>assets/img_for_vendor/VENDOR-MANAGEMENT.jpg" width="60%">
    </div>
  </div>
</center><br><br>
<p><div align="left"><h3 style="color:#20B2AA">Reason why to choose NEXTCLICK:</h3></div></p><br>
<center>
  <div>
    <div align="left">
      <img src="<?php echo base_url();?>assets/img_for_vendor/vendor-risk-management.jpg" width="100%">
    </div>
  </div>
</center><br>
<center>
  <div>
    <div align="left">
      <img src="<?php echo base_url();?>assets/img_for_vendor/new.jpg" width="100%"><br>
    </div>
  </div>
</center>
    </section>
 
</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- General JS Scripts -->
<script src="http://vendor.nextclick.in/assets/home/js/contact-form.js"></script>
</div>
</main>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://static.codepen.io/assets/common/stopExecutionOnTimeout-157cd5b220a5c80d4ff8e0e70ac069bffd87a61252088146915e8726e5d9f147.js"></script>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
<script id="rendered-js" >
$(document).ready(function () {
  var $default2 = $("#default2");
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100 && $default2.hasClass("navbar-light bg-light default2")) {
      $default2.removeClass("navbar-light bg-light default2").addClass("navbar-dark bg-dark fixed-top");
    } else if ($(this).scrollTop() <= 100 && $default2.hasClass("navbar-dark bg-dark fixed-top")) {
      $default2.removeClass("navbar-dark bg-dark fixed-top").addClass("navbar-light bg-light default2");
    }
  }); //scroll
});

$(document).ready(function () {
  var $default3 = $("#nav-size");
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100 && $default3.hasClass("container-fuil")) {
      $default3.removeClass("container-fuil").addClass("container");
    } else if ($(this).scrollTop() <= 100 && $default3.hasClass("container")) {
      $default3.removeClass("container").addClass("container-fuil");
    }
  }); //scroll
});

jQuery(window).scroll(function () {
  if (jQuery(window).scrollTop() > 100) {
    jQuery('#logo-small').css('display', 'block');
  } else {
    jQuery('#logo-small').css('display', 'none');
  }
});
jQuery(window).scroll(function () {
  if (jQuery(window).scrollTop() > 100) {
    jQuery('#nav-size').css('display', 'block');
    jQuery('#nav-size-small').css('display', 'none');
  } else {
    jQuery('#nav-size').css('display', 'block');
    jQuery('#nav-size-small').css('display', 'none');
  }
});
  </script>
</body>
</html>
 
