<?php

class Payment extends MY_Controller
{

    public function __construct()
    {
        error_reporting(E_ERROR | E_PARSE);
        parent::__construct();
        $this->load->library('pagination');
        $this->template = 'template/admin/main';
        $this->load->model('vendor_list_model');
        $this->load->model('wallet_transaction_model');
        $this->load->model('user_model');
        $this->load->model('user_account_model');
        $this->load->helper('common');
    }

    public function wallet_transactions($type = 'list', $rowno = 0)
    {
        if ($type == 'list') {
            $this->load->library('pagination');
            $rowperpage = $noofrows ? $noofrows : 10;
            if ($rowno != 0) {
                $rowno = ($rowno - 1) * $rowperpage;
            }
            $this->data['title'] = 'Transactions List';
            $this->data['content'] = 'payment/list';
            $this->data['nav_type'] = 'Transactions';
            $allcount = $this->db->query("SELECT wt.*, u.username FROM wallet_transactions AS wt
           JOIN users AS u ON wt.account_user_id  = u.id WHERE wt.status = '1'")->num_rows();

            $this->data['transactions'] = $this->db->query("SELECT wt.*, u.username FROM wallet_transactions AS wt JOIN users AS u ON wt.account_user_id  = u.id WHERE wt.status = '1' ORDER BY wt.created_at DESC LIMIT " . $rowno . ',' . $rowperpage)->result_array();

            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] = "</ul>";
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='page-item active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tagl_close'] = "</li>";
            $config['base_url'] = base_url() . 'payment/payment/wallet_transactions/list';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $allcount;
            $config['per_page'] = $rowperpage;
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $this->data['row'] = $rowno;
            $this->data['noofrows'] = $rowperpage;
            $this->_render_page($this->template, $this->data);
        } else if ($type == 'c') {

            $this->data['title'] = 'Transactions List';
            $this->data['content'] = 'payment/create_transations';
            $this->data['nav_type'] = 'Create Transactions';
            $this->data['user'] = $this->db->query("SELECT * from users WHERE status = '1' order by 'DESC'")->result_array();
            $this->_render_page($this->template, $this->data);
        } else if ($type == 'st') {

            $uid = $this->input->post('userid');
            $tr = $this->db->query("SELECT * FROM users WHERE id = '$uid'")->row();
            echo json_encode($tr);
        } else if ($type == 'srh') {

            $searchText = $this->input->post('search');
            $this->data['user'] = $this->db->query("SELECT phone,id FROM users where phone like '%" . $searchText . "%' limit 5")->result_array();
            foreach ($this->data['user'] as $a) {
                $phone = $a['phone'];
                $id = $a['id'];
                $search_arr[] = array(
                    "id" => $id,
                    "phone" => $phone
                );
            }
            echo json_encode($search_arr);
        } else if ($type == 'e') {

            $user_id = $this->input->post('id');
            $amount = $this->input->post('amount');
            $type1 = 'DEBIT'; // $this->input->post('modetype');
            $message = $this->input->post('message');
            $walletamount = $this->input->post('walletamount');

            if ($walletamount > 0) {
                $txn_id = 'NC-' . generate_trasaction_no();
                $tr = $this->user_model->payment_update($user_id, $amount, $type1, $wallet_type = 'wallet', $txn_id, $order_id = NULL, $message);
            } else {
                $this->session->set_flashdata('error', 'Cannot Add below 0 amount');
                redirect('payment/wallet_transactions/list/0');
            }
            /*
             * $user_id = $this->session->userdata('user_id');
             * if($walletamount > 0)
             * {
             * $tr = $this->user_model->payment_update($user_id, $amount, $type1, $wallet_type = 'wallet', $txn_id = 'L200453212', $order_id = NULL,$message);
             * }
             */
            if ($tr) {
                $this->session->set_flashdata('error', 'Successfully Added');
                redirect('payment/wallet_transactions/list/0');
            } else {
                redirect('payment/wallet_transactions/c/0');
            }
        } else if ($type == 'refund') {

            $id = base64_decode(base64_decode($this->input->get('id')));

            $this->data['title'] = 'Transactions List';
            $this->data['content'] = 'payment/create_transations';
            $this->data['nav_type'] = 'Create Transactions';
            $this->data['refnd'] = 'refunds';
            $this->data['dt'] = $this->db->query("SELECT * FROM ecom_orders WHERE id = '$id'")->result_array();
            $uid = $this->data['dt'][0]['created_user_id'];
            $this->data['user'] = $this->db->query("SELECT * FROM users WHERE id = '$uid' AND status = '1'")->result_array();
            $this->_render_page($this->template, $this->data);
        }
    }

    public function wallet_refunds($type = 'list', $rowno = 0)
    {
        if ($type == 'list') {

            $rowperpage = $noofrows ? $noofrows : 10;
            if ($rowno != 0) {
                $rowno = ($rowno - 1) * $rowperpage;
            }

            $this->data['title'] = 'Wallet Refunds';
            $this->data['content'] = 'payment/wallet_refunds';
            $this->data['nav_type'] = 'Wallet Refunds';

            $allcount = $this->db->query("SELECT eo.track_id,eo.total, u.username,u.unique_id,u.first_name,u.last_name,u.email,u.phone FROM ecom_orders AS eo
           JOIN users AS u ON eo.created_user_id  = u.id WHERE eo.order_status_id = '17'")->num_rows();

            $this->data['transactions'] = $this->db->query("SELECT eo.track_id,eo.total, u.username,u.unique_id,u.first_name,u.last_name,u.email,u.phone FROM ecom_orders AS eo
           JOIN users AS u ON eo.created_user_id  = u.id WHERE eo.order_status_id = '17'")->result_array();

            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] = "</ul>";
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='page-item active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tagl_close'] = "</li>";
            $config['base_url'] = base_url() . 'payment/payment/wallet_transactions/list';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $allcount;
            $config['per_page'] = $rowperpage;
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $this->data['row'] = $rowno;
            $this->data['noofrows'] = $rowperpage;
            $this->_render_page($this->template, $this->data);
        }
    }

    public function admin_wallet_reports($rowno = 0)
    {
        $noofrows = $this->filter_config();

        $rowperpage = $noofrows ? $noofrows : 10;
        if ($rowno != 0) {
            $rowno = ($rowno - 1) * $rowperpage;
        }
        $user_id = $this->config->item('super_admin_user_id', 'ion_auth');
        $allcount = $this->wallet_transaction_model->all($rowperpage, $rowno, $user_id, $this->data['start_date'], $this->data['end_date'], NULL, NULL, NULL, NULL, TRUE);
        $this->data['wallet_details'] = $this->user_account_model->where('user_id', $user_id)->get();
        $this->data['transactions'] = $this->wallet_transaction_model->all($rowperpage, $rowno, $this->config->item('super_admin_user_id', 'ion_auth'), $this->data['start_date'], $this->data['end_date'], NULL, NULL, NULL, NULL, FALSE);
        if ($this->data['transactions']) {
            foreach ($this->data['transactions'] as $key => $txn) {
                $this->data['transactions'][$key]['user_account'] = $this->user_model->fields('id, display_name, phone, first_name')
                    ->where('id', $txn['account_user_id'])
                    ->get();
            }
        } else {
            $this->data['transactions'] = [];
        }
        $url = base_url() . 'payment/admin_wallet_reports';
        $this->pagination_config($allcount, $rowperpage, $url);

        $this->data['title'] = 'Admin wallet reports';
        $this->data['content'] = 'admin_wallet_reports';
        $this->data['nav_type'] = 'payment_reports';
        // print_array($this->data);
        $this->_render_page($this->template, $this->data);
    }

    public function admin_earnings_wallet()
    {}

    public function admin_floating_wallet()
    {}

    public function admin_income_wallet()
    {}

    public function filter_config()
    {
        $search_text = "";
        $noofrows = 10;
        if ($this->input->post('submit') != NULL) {
            $search_text = $this->input->post('q');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            $noofrows = $this->input->post('noofrows');
            $this->session->set_flashdata(array(
                "q" => $search_text,
                'noofrows' => $noofrows,
                'start_date' => $start_date,
                'end_date' => $end_date
            ));
        } else {
            if ($this->session->flashdata('q') != NULL || $noofrows != NULL || $this->session->flashdata('start_date') != NULL || $this->session->flashdata('end_date') != NULL) {
                $search_text = $this->session->flashdata('q');
                $noofrows = $this->session->flashdata('noofrows');
                $start_date = $this->session->flashdata('start_date');
                $end_date = $this->session->flashdata('end_date');
            }
        }

        $this->data['q'] = $search_text;
        $this->data['noofrows'] = $noofrows;
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        return $noofrows;
    }

    public function pagination_config($allcount, $rowperpage, $url)
    {
        $rowperpage = $noofrows ? $noofrows : 10;
        if ($rowno != 0) {
            $rowno = ($rowno - 1) * $rowperpage;
        }

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='page-item active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tagl_close'] = "</li>";
        $config['base_url'] = $url;
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
    }
}

