<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class Product extends MY_Controller
{

    function __construct()
    {
        error_reporting(E_ERROR | E_PARSE);
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) 
            redirect('auth/login');

        $this->load->model('food_item_model');
        $this->load->model('food_section_model');
        $this->load->model('food_sec_item_model');
        $this->load->model('user_model');
        $this->load->model('vendor_list_model');
        $this->load->model('food_item_image_model');
        $this->load->model('sub_category_model');
        $this->load->model('food_menu_model');
    }

    /**
     * @desc To update products from excel sheet along with images zip
     * @author Mehar
     * @dt 30/08/2021
     */
    public function product_bulk_upload(){
            $zip_file = $_FILES['images_zip'];
            $excel_file = $_FILES['excel_file'];
            $base_path = dirname(BASEPATH).'/uploads/products_zip/';
            if (! file_exists($base_path)) {
                mkdir($base_path, 0777, true);
            }
            
            $random_digit= time();
            $new_file_name = $random_digit.".zip";
            mkdir($base_path.$random_digit, 0777, true);
            
            $zip_file_path = $base_path.$random_digit.'/'.$new_file_name;
            if(copy($zip_file['tmp_name'], $zip_file_path)){
                // zip extraction
                $zip = new ZipArchive();
                if ($zip->open($zip_file_path) === TRUE) {
                    $zip->extractTo($base_path.$random_digit);
                    $zip->close();
                    $is_updated = $this->upload_excel_with_images($excel_file, $base_path, $random_digit);
                }else {
                    $this->session->set_flashdata('upload_status', ["error" => "Unable to extract ZIP"]);
                }
                
            } else {
                $this->session->set_flashdata('upload_status', ["error" => "Uploading ZIP is failed"]);
            }
            $this->data['title'] = 'Product bulk upload';
            $this->data['content'] = 'food/product/upload_page';
            $this->data['nav_type'] = 'product_upload';
            $this->_render_page($this->template, $this->data);
    }
    
    public function upload_excel_with_images($file, $base_path, $random_digit){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        // If file uploaded
        if(!empty($file['name'])) {
            // get file extension
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            
            if($extension == 'csv'){
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } elseif($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }
            // file path
            $spreadsheet = $reader->load($file['tmp_name']);
            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            
            // array Count
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('sub_cat_id', 'menu_id', 'brand_id','name', 'desc', 'availability', 'status', 'varinat_names', 'varinat_prices', 'variant_weights');
            $makeArray = array('sub_cat_id' => 'sub_cat_id', 'menu_id' => 'menu_id', 'brand_id' => 'brand_id', 'name' => 'name', 'desc' => 'desc', 'availability' => 'availability', 'status' => 'status', 'varinat_names' => 'varinat_names', 'varinat_prices' => 'varinat_prices', 'variant_weights' => 'variant_weights');
            $SheetDataKey = array();
            
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    }
                }
            }
            //print_array($SheetDataKey);
            $dataDiff = array_diff_key($makeArray, $SheetDataKey);
            if (empty($dataDiff)) {
                $flag = 1;
            }
            // match excel sheet column
            if ($flag == 1) { $k = 0;
            for ($i = 2; $i <= $arrayCount; $i++) {
                
                $sub_cat_id = $SheetDataKey['sub_cat_id'];
                $menu_id = $SheetDataKey['menu_id'];
                $brand_id = $SheetDataKey['brand_id'];
                $name = $SheetDataKey['name'];
                $desc = $SheetDataKey['desc'];
                $availability = $SheetDataKey['availability'];
                $status = $SheetDataKey['status'];
                $varinat_names = $SheetDataKey['varinat_names'];
                $varinat_prices = $SheetDataKey['varinat_prices'];
                $variant_weights = $SheetDataKey['variant_weights'];
                
                $sub_cat_id = filter_var(trim($allDataInSheet[$i][$sub_cat_id]), FILTER_SANITIZE_STRING);
                $menu_id = filter_var(trim($allDataInSheet[$i][$menu_id]), FILTER_SANITIZE_STRING);
                $brand_id = filter_var(trim($allDataInSheet[$i][$brand_id]), FILTER_SANITIZE_STRING);
                $name = filter_var(trim($allDataInSheet[$i][$name]), FILTER_SANITIZE_STRING);
                $desc = filter_var(trim($allDataInSheet[$i][$desc]), FILTER_SANITIZE_EMAIL);
                $availability = filter_var(trim($allDataInSheet[$i][$availability]), FILTER_SANITIZE_STRING);
                $status = filter_var(trim($allDataInSheet[$i][$status]), FILTER_SANITIZE_STRING);
                $varinat_names = filter_var(trim($allDataInSheet[$i][$varinat_names]), FILTER_SANITIZE_STRING);
                $varinat_prices = filter_var(trim($allDataInSheet[$i][$varinat_prices]), FILTER_SANITIZE_STRING);
                $variant_weights = filter_var(trim($allDataInSheet[$i][$variant_weights]), FILTER_SANITIZE_STRING);
                
                $sounds_like = $this->sounds_like($name, $sub_cat_id, $menu_id);
                    if(! empty($name)  &&  ! empty($sub_cat_id)){
                        $item_id = $this->food_item_model->insert([
                            'sub_cat_id' => $sub_cat_id,
                            'menu_id' => $menu_id,
                            'brand_id' => $brand_id,
                            'product_code' => implode('-', str_split(substr(strtoupper(md5(time() . rand(1000, 9999))), 0, 20), 4)),
                            'name' => $name,
                            'desc' => $desc,
                            'sounds_like' => $sounds_like,
                            'availability' => 1,
                            'status' => 1
                        ]);
                        
                        if ($item_id) {
                            $section_id = $this->food_section_model->insert([
                                'menu_id' => $menu_id,
                                'item_id' => $item_id,
                                'name' => $name
                            ]);
                            
                            $varinat_names_array = explode(',', $varinat_names);
                            $varinat_price_array = explode(',', $varinat_prices);
                            $varinat_weight_array = explode(',', $variant_weights);
                            
                            if ($section_id && ! empty($varinat_names_array)) {
                                $section_items = [];
                                foreach ($varinat_names_array as $k => $value){
                                    array_push($section_items, [
                                        'menu_id' => $menu_id,
                                        'item_id' => $item_id,
                                        'sec_id' => $section_id,
                                        'section_item_code' => implode('-', str_split(substr(strtoupper(md5(time() . rand(1000, 9999))), 0, 20), 4)),
                                        'name' => $varinat_names_array[$k],
                                        'price' => $varinat_price_array[$k],
                                        'weight' => $varinat_weight_array[$k],
                                        'status' => 1
                                    ]);
                                }

                                $this->food_sec_item_model->insert($section_items);
                                $file_name = str_replace(array("#", "'", ";","@[^0-9a-zA-Z.]+"), '', $name);
                                $space_file_name = str_replace('  ', ' ', $file_name);
                                $image_file_name = strtolower(str_replace(' ', '_', $space_file_name)).'.jpg';
                                if(file_exists($base_path.$random_digit.'/nxcproducts/'.$image_file_name)){
                                    $product_image_id = $this->food_item_image_model->insert([
                                        'item_id' => $item_id,
                                        'serial_number' => 1,
                                        'ext' => 'jpg'
                                    ]);
                                    if (! file_exists('./uploads/food_item_image/')) {
                                        mkdir('./uploads/food_item_image/', 0777, true);
                                    }
                                    $source_image = file_get_contents($base_path.$random_digit.'/nxcproducts/'.$image_file_name);
                                    file_put_contents('./uploads/food_item_image/'."food_item_" . $product_image_id . ".jpg", $source_image);
                                }else {
                                    // $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check name and image name"]);
                                    // break;
                                }
                    }else{
                        $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)---Please check email and mobile"]);
                        break;
                    }
                }
                    
                    $this->session->set_flashdata('upload_status', ["success" => "Products successfully imported..!"]);
                }else{
                    $this->session->set_flashdata('upload_status', ["error" => "Error occured at row no($i)"]);
                    $this->data['vendor'] = array('sub_cat_id' => $sub_cat_id, 'menu_id' => $menu_id, 'brand_id' => $brand_id, 'name' => $name, 'desc' => $desc, 'availability' => $availability, 'status' => $status, 'varinat_names' => $varinat_names, 'varinat_prices' => $varinat_prices, 'variant_weights' => $variant_weights);
                    break;
                }
            }
            } else {
                $this->session->set_flashdata('upload_status', ["error" => "Please import correct file, did not match excel sheet column"]);
            }
        }
    }
    
    /**
     * To generate phonatic sounds which helps us to search more accurately
     *
     * @author Mehar
     *
     * @param string $name
     * @param integer $menu_id
     * @param integer $cat_id
     * @param integer $sub_cat_id
     * @param integer $brand_id
     * @return string
     */
    public function sounds_like($name = NULL, $shop_by_cat_id = NULL, $menu_id = NULL)
    {
        $sounds_like = '';
        if (! is_null($menu_id)) {
            $menu_name = $this->food_menu_model->fields('name')
            ->where('id', $menu_id)
            ->get();
            $sounds_like .= metaphone($menu_name['name']) . ' ';
        }
        if (! is_null($shop_by_cat_id)) {
            $cat_name = $this->sub_category_model->fields('name')
            ->where('id', $shop_by_cat_id)
            ->get();
            $sounds_like .= metaphone($cat_name['name']) . ' ';
        }
        if (! is_null($name)) {
            $sounds_like .= metaphone($name) . ' ';
        }
        return $sounds_like;
    }
    
}