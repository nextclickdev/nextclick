<?php

class Notifications_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'notifications';
        $this->primary_key = 'id';

        $this->_config();
        $this->_form();
        $this->_relations();
    }
   
    public function _config() {
        $this->timestamps = FALSE;
        $this->soft_deletes = FALSE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
        $this->has_one['user'] = array('User_model', 'id', 'user_id');
        $this->has_one['order'] = array('Ecom_order_model', 'id', 'ecom_order_id');
        $this->has_one['pickup_order'] = array('Pickup_orders_model', 'id', 'pickup_order_id');
    }

    public function _form(){
        $this->rules = array(
            array (
                'lable' => 'User',
                'field' => 'user_id',
                'rules' => 'required'
            ),
            array (
                'lable' => 'Title',
                'field' => 'title',
                'rules' => 'required'
            ),
            array (
                'lable' => 'Description',
                'field' => 'desc',
                'rules' => 'required'
            ),
        );
    }

    public function getAdminNotifications(){
        $this->load->model('manualpayment_model');
        $adminNotificationSummary = [];
        $manualPayments = [];
        $overallCount = 0;
        $manualPayments = $this->manualpayment_model->getPendingPayments();
        $overallCount+= count($manualPayments);
        array_push($adminNotificationSummary, [
            'title'=>"Manual Payments",
            'key'=>"manual_payments",
            'count'=>count($manualPayments)
        ]);
        return [
            "overall_count" => $overallCount,
            "data"=> $adminNotificationSummary
        ];
    }
}

